package app;

import model.Network;
import wine.Wine;

import java.util.logging.Logger;

public class App {

    private static final Logger logger = Logger.getLogger(App.class.getName());

    public static void main(String[] args) {
        Wine wineTest = new Wine();
        Network<Integer>.Results results = wineTest.classify();
        String string = (results.correct + " correct of " + results.trials + " = " +
                results.percentage * 100 + "%");

        logger.info(string);
    }
}
